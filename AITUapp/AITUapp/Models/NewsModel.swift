//
//  NewsModel.swift
//  AITUapp
//
//  Created by Nurba Seyilkhan on 15.06.2021.
//

import Foundation


// MARK: - Welcome
struct News: Decodable {
    let articles: [Article]


// MARK: - Article
struct Article: Decodable {
    let title: String?
    let urlToImage: String?
 
 


    init(news: NewsEntity) {
        self.title = news.title
        self.urlToImage = news.image
     
    }

}
}
